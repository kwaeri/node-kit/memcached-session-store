/**
 * SPDX-PackageName: kwaeri/memcached-session-store
 * SPDX-PackageVersion: 1.0.0
 * SPDX-FileCopyrightText: © 2014 - 2022 Richard Winters <kirvedx@gmail.com> and contributors
 * SPDX-License-Identifier: Apache-2.0 WITH LLVM-exception OR MIT
 */


'use strict'


// INCLUDES
import { Configuration } from '@kwaeri/configuration';
import {
    ClientSession,
    SessionStore,
    BaseSessionStore,
    SessionBits
} from '@kwaeri/session-store';
import Memcached from 'memcached';
import { kdt } from '@kwaeri/developer-tools';
import debug from 'debug';


// DEFINES
const _ = new kdt(),
      DEBUG = debug( 'nodekit:memcached-session-store' );

export type MemcachedAPIPromise = {
    result?: boolean;
    data: any;
};


/**
 * Memcached Session Store
 *
 * Manages a 'store' object, where each memcached key is a session
 * named after its 'ID'. For each key, an object that is the session,
 * and all of its associated information, is stored within a JSON string.
 */
export class MemcachedSessionStore implements SessionStore {
    /**
     * @var { string }
     */
    public version: string;

    /**
     * @var { string }
     */
    public type: string;

    /**
     * @var { any }
     */
    protected configuration: any;

    /**
     * @var { Configuration }
     */
    private memcachedConfiguration: Configuration;

    /**
     * @var { any }
     */
    private memcachedConf?: any;

    /**
     * @var { Memcached }
     */
    private store?: Memcached;

    /**
     * Class constructor
     *
     * @param { any } store The store interface. In this case, a filesystem IO interface
     * @param { any } configuration The session store configuration
     *
     * @returns { void }
     *
     * @since 0.1.0
     */
    constructor( configuration: any ) {

        // Organize all of the uncertainty:
        this.version     = ( configuration && configuration.version ) ? configuration.version : null,
        this.type        = ( configuration && configuration.type ) ? configuration.type : "Database";

        // Pack the store interface into the configuration:
        //configuration = _.set( configuration, 'store', new Memcached( _.get( configuration, 'locations', 'localhost:11211' ) ) );
        //this.store       = new Memcached( _.get( configuration, 'locations', '127.0.0.1:11211' ) );

        // This is just the session configuration from conf/
        this.configuration = configuration;

        this.memcachedConfiguration = new Configuration( 'conf', `memcached.${configuration.environment}.json` );

        // Call the parent class:
        //super( configuration );
    }


    /**
     * Gets the memcached server configuration
     *
     * @param { void }
     *
     * @returns { Promise<Configuration> } the promise for a {@link Configuration} object
     */
    private async getConf(): Promise<any> {
        if( this.memcachedConf === undefined ) {
            const memcachedConf = await this.memcachedConfiguration.get();

            if( !memcachedConf )
                    return Promise.reject( new Error( `[MEMCACHED_SESSION_STORE][GET_CONF] There was an issue reading the memcached configuration. ` ) );

            DEBUG( `Memcached configuration successfully fetched:` );
            DEBUG( memcachedConf );

            this.memcachedConf = memcachedConf;
        }

        // Otherwise, return the conf
        return Promise.resolve( this.memcachedConf );
    }


    /**
     * Get the store; If store is undefined` then attempt to create it, and reject the
     * promise in the event that this method fails.
     *
     * @returns { Promise<Memcached> } the promise for a {@link Memcached} object
     */
    private async getClient(): Promise<Memcached> {
        try{
            if( !this.store ) {
                DEBUG( `Get memcached client` );

                const conf = await this.getConf();

                const store = new Memcached( conf.locations ); //this.dbo = new Driver( await this.getConf( true ), this.databaseProvider ).get();

                if( !store )
                    return Promise.reject( new Error( `There was an issue getting the Memcached client` ) );

                this.store = store;
            }

            return Promise.resolve( this.store! );
        }
        catch( error ) {
            DEBUG( `${error}` );

            return Promise.reject( `${error}` );
        }
    }


    /**
     * A method to read a Sesssion from the memcached server
     *
     * @param { string } id The id of the session to use as a key value to read from
     *
     * @return { Promise<ClientSession|boolean|null> } the promise for a {@link ClientSession}, or `null`.
     */
    private async read( id: string ): Promise<ClientSession|null> {
        DEBUG( `Read the session '%s'`, id );

        const store = await this.getClient();

        // If everything 'exists', Read in the configuration:
        return new Promise(
            ( resolve, reject ) => {
                store.get(
                    id,
                    ( error: any, data: any ) => {
                        if( error )
                            reject( new Error( `[MEMCACHED_SESSION_STORE]: There was an issue reading the session from '${id}': ${error}. ` ) );

                        DEBUG( `Parse session '%s' read from memcached'`, id );

                        // Parse the file's contents into our KwaeriConfiguration:
                        if( data && data.length ) {
                            const session = JSON.parse( data as any );

                            // Log it for now for proof:
                            DEBUG( `Session: %o`, session );

                            resolve( session );
                        }
                        else {
                            DEBUG( `Data: %o`, data );

                            resolve( null );
                        }
                    }
                );
            }
        );
    }


    /**
     * A method to write the session to the memcached server
     *
     * @param { any } id The name of the session to use as a key
     * @param { any } session The session data to store with the new key
     *
     * @returns { Promise<boolean> } the promise for a `boolean`
     */
    private async write( id: string, session: SessionBits, expires: number = 900 ): Promise<boolean> {
        DEBUG( `Write session '%s'`, id );

        const store = await this.getClient();

        return new Promise(
            ( resolve, reject ) => {
                store.set(
                    id,
                    JSON.stringify( session ),
                    expires,
                    ( error: any ) => {
                        if( error )
                            reject( new Error( `[MEMCACHED_SESSION_STORE]: There was an issue writing the seesion '${id}' to the memcached server: ${error}. ` ) );

                        resolve( true );
                    }
                );
            }
        );
    }


    /**
     * A method to update the session in the memcached server
     *
     * @param { any } id The name of the session as a key
     * @param { any } session The session data to replace for the existing key
     *
     * @returns { Promise<boolean> } the promise for a `boolean`
     */
    private async update( id: string, session: any, expires: number = 900 ): Promise<boolean> {
        DEBUG( `Update session '%s'`, id );

        const store = await this.getClient();

        return new Promise(
            ( resolve, reject ) => {
                store.replace(
                    id,
                    JSON.stringify( session ),
                    expires,
                    ( error: any ) => {
                        if( error )
                            reject( new Error( `[MEMCACHED_SESSION_STORE]: There was an issue replacing the seesion '${id}' on the memcached server: ${error}. ` ) );

                        resolve( true );
                    }
                );
            }
        );
    }


    /**
     * Method to delete a session from the memcached server
     *
     * @param { string } id The session id the key/value pair being deleted is named after
     *
     * @returns { Promise<boolean> } the promise for a `boolean`
     */
    public async delete( id: string ): Promise<boolean> {
        DEBUG( `Delete session '%s'`, id );

        const store = await this.getClient();

        return new Promise(
            ( resolve, reject ) => {
                store.del(
                    id,
                    ( error: any ) => {
                        if( error )
                            reject( new Error( `[MEMCACHED_SESSION_STORE] There was an issue deleting the session '${id}' on the memcached server: ${error}. ` ) );

                        resolve( true );
                    }
                )
            }
        );
    }


    /**
     * Creates a new client session
     *
     * @param { string } id The id of the session to create
     * @param { any } clientSession The object representation of a client session
     *
     * @returns { ClientSession|null } the promise for a {@link ClientSession} or `null`
     */
    public async createSession( id: string, sessionBits: SessionBits = {} as SessionBits ): Promise<ClientSession> {
        try {
            DEBUG( `Create session '%s'`, id );

            // When we want to create a session, first write the data:
            if( !await this.write( id, sessionBits ) )
                DEBUG( `There was an issue creating session '%s': the session could not be written to the memcached server`, id );

            DEBUG( `Read session '%s' after write`, id );

            // Then return the read session, to ensure it was written, etc:
            const deferredRead = await this.read( id ) as ClientSession;

            //return this.store[id];
            return Promise.resolve( deferredRead );
        }
        catch( error ) {
            DEBUG( `[CREATE]: Error: %o`, error );

            return Promise.reject( error );
        }
    }


    /**
     * Returns an object from the memcached server representing a specific session
     *
     * @param { string | number } id The id of the session requested
     *
     * @returns { ClientSession } the promise for a {@link ClientSession}, or `null`
     */
    public async getSession( id: string ): Promise<ClientSession|null> {
        try {
            DEBUG( `Get session '%s'`, id );

            const deferredRead = await this.read( id );

            DEBUG( `Session found and read: ['%o']`, deferredRead );

            //return ( ( this.store.hasOwnProperty( id ) ) ? this.store[id] : false );
            return Promise.resolve( ( ( deferredRead ) ? deferredRead : null ) );
        }
        catch( error ) {
            DEBUG( `[GET]: Error: %o`, error );

            return Promise.reject( error );
        }
    };


    /**
     * Removes the specified session from memory
     *
     * @param { string } id The id of the session to delete
     *
     * @returns { boolean } the promise for a `boolean`
     */
    public async deleteSession( id: string ): Promise<boolean> {
        try{
            const found = await this.read( id );

            if( !found ) {
                DEBUG( `There was an issue deleting session '%s': The session could not be read`, id );

                return Promise.resolve( false );
            }

            DEBUG( `Delete session '%s'`, id );

            const deferredDelete = await this.delete( id );

            return Promise.resolve( deferredDelete );
        }
        catch( error ) {
            DEBUG( `[DELETE]: Error: %o`, error );

            return Promise.reject( error );
        }
    }


    /**
     * Gets a count of existing sessions. Since memcached does not support counting, 0 is always returned.
     *
     * @param void
     *
     * @returns { Promise<0> } the promise for the `number` `0`
     */
    public async countSessions(): Promise<any> {
        DEBUG( `[COUNT]: Cannot count sessions: Memcached does not allow the counting of keys by design` );

        return Promise.resolve( 0 );
    }


    /**
     * Deletes any sessions which have gone beyond their expiration.
     *
     * **NOTE**: Since Memcached expires its cache automatically by design, there is no need to implement
     * a functional `cleanSessions()`. However, because the session provider does manage session expiration
     * itself, we need to fool its interface by providing a method it can call.
     *
     * @param void
     *
     * @returns { Promise<void> } `void`
     */
    public async cleanSessions(): Promise<void> {
        DEBUG( `[CLEAN]: Cannot clean sessions: Memcached cache expires automatically by design` );

        return Promise.resolve();
    }


    /**
     * Gets the specified value from the specified session
     *
     * @param { string | number } id The id of the session the value is being requested from
     * @param { string } name The name of the session value that is being requested
     * @param { any } defaultValue A default value in the event the property being requested is unknown or unset
     *
     * @returns { any }
     */
    public async get( id: string, name: string, defaultValue: any ): Promise<any> {
        try{
            DEBUG( `Get '%s' from session '%s'`, name, id );

            const deferredRead = await this.read( id ) as ClientSession;

            if( !deferredRead ) {
                DEBUG( `Error getting '%s' from session '%s'`, name, id );

                Promise.reject( new Error( `[MEMCACHED_SESSION_STORE] When getting '${name}', the session '${id}' could not be read. ` ) );
            }

            return Promise.resolve( _.get( deferredRead, name, null ) );
        }
        catch( error ) {
            DEBUG( `[DELETE]: Error: %o`, error );

            return Promise.reject( error );
        }
    }


    /**
     * Sets the specified value of the specified session
     *
     * @param { string: number } id The id of the session for which the value is being set
     * @param { string } name The name of the property for the specified session for which the value is being set
     * @param { any } value The value being set for the specified property of the specified session
     *
     * @returns { any }
     */
    public async set( id: string, name: string, value: any ): Promise<any> {
        try{
            DEBUG( `Set '%s' to '%o' for session '%s'`, name, value, id );

            const deferredRead = await this.read( id ) as ClientSession;

            if( !deferredRead ) {
                DEBUG( `Error setting '%s' to '%o' for session '%s'`, name, value, id );

                Promise.reject( new Error( `[MEMCACHED_SESSION_STORE] When setting '${name}' to '${value}', the session '${id}' could not be found. ` ) );
            }

            const session = _.set( deferredRead, name, value );

            const deferredWrite = await this.update( id, session );

            if( !deferredWrite ) {
                DEBUG( `Error setting '%s' to '%o', for session '%s': session could not be stored with the memcached server`, name, value, id );

                Promise.reject( new Error( `[MEMCHACED_SESSION_STORE] When setting '${name}' to '${value}', the session '${id}' could not be stored with the memcached server. ` ) );
            }

            return Promise.resolve( session[name] );
        }
        catch( error ) {
            DEBUG( `[DELETE]: Error: %o`, error );

            return Promise.reject( error );
        }
    }
}